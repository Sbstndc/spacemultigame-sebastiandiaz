﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using FSMHelper;

public class SimonSaysBehaviour : MonoBehaviour
{
    // keep an instance of our state machine
    private SimonSaysBehaviourStateMachine m_SimonSaysSM = null;

    //Public variables_Unity Editor:
    public GameObject[] ssb_lights;//arreglo de luces, objeto de juego
    public AudioSource[] ssb_sounds;//arreglo de sonidos, clips de audioss
    public Text info; // variable de tipo texto
    public AudioClip[] ssb_errorSound;
    //    private GameObject sound;

    //[HideInInspector]
    public List<int> sequence = new List<int>();
    //[HideInInspector]
    public int currentSequenceIndex = 0;
    //[HideInInspector]
    public int buttonPressed;

    void Start()
    {
        Debug.Log("void Start()");
        // create the state machine and start it
        m_SimonSaysSM = new SimonSaysBehaviourStateMachine(this.gameObject);
        m_SimonSaysSM.StartSM();
    }

    void Update()
    {
        // update the state machine very frame
        m_SimonSaysSM.UpdateSM();

        // this is how you can print the current active state tree to the log for debugging
        if (Input.GetKeyDown(KeyCode.Space))
        {
            m_SimonSaysSM.PrintActiveStateTree();
        }
    }

    void OnDestroy()
    {
        // stop the state machine to ensure all the Exit() gets called
        if (m_SimonSaysSM != null)
        {
            m_SimonSaysSM.StopSM();
            m_SimonSaysSM = null;
        }
    }

    public void PressButton(int index)
    {
        Debug.Log("PressButton");
        if (index >= ssb_lights.Length)
        {
            Debug.LogError("PressButton: index(" + index + ") mes gran que el nombre de butons");
            //  errorSound = GetComponent<AudioSource>().PlayOneShot();

            return;
        }
        //Apagar primer totes les llums:
        foreach (GameObject light in ssb_lights)//recorrer el vector
        {
            light.SetActive(false);//apagar todas las luces del arreglo
        }

        ssb_lights[index].SetActive(true);//prender todas las luces del vector segun index
        // GetComponent<AudioSource>().PlayOneShot(ssb_sounds[index]);

        //GetComponent<AudioSource>().PlayOneShot(sounds[index]);
        //sound = GameObject.FindGameObjectWithTag("SSB")
        // conseguir el componente de audio source y re[rpducirlo una vez, segun el index
    }

    public void ClickOnButton(int index)//funcion para captar el lcick del boton y pasarlo a index
    {
        if (index >= ssb_lights.Length)//si index mayor o igual a longitud de vector light
        {
            Debug.Log("ClickOnButton");
            Debug.LogError("PressButton: index(" + index + ") mes gran que el nombre de butons");
            // GetComponent<AudioSource>().PlayOneShot();
            return;
        }
        m_ssb_lOff();//llamar a funcion de apagadode luces
        ssb_lights[index].SetActive(true);//prender la luz indicada por index
        //GetComponent<AudioSource>().PlayOneShot(ssb_sounds[index]);
        buttonPressed = index; // boton presionado igual a numeor de index
    }

    public void m_ssb_lOff()//m_ssb_lOff lightoff() //funcion de apagado de luces del vector
    {
        foreach (GameObject light in ssb_lights)//recorrido de vector de objeto luz en vector
        {
            light.SetActive(false);//desactivar luces del vector
        }
    }

    //internal void m_ssb_Wtp(int index) // funcion para hcer ciclo randomico infinito mientras espera a precionar las luces
    //{
    //    while()
    //    {
    //        foreach (GameObject light in ssb_lights)//recorrido de vector de objeto luz en vector
    //        {
    //            int sss_wtp_nB = Random.Range(0, 4);
    //            ssb_lights[index].SetActive(true);
    //        }
    //    }
    //}

}
