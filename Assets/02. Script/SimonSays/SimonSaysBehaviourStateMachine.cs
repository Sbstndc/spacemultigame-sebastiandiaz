﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using FSMHelper;

public class SimonSaysBehaviourStateMachine : FSMStateMachine
{
    public GameObject m_GameObject = null; // inicializar variable game object como vacia
    public SimonSaysBehaviour m_ssb = null; // m_ssb como una variable nula

    public SimonSaysBehaviourStateMachine(GameObject characterObj) //funcion para cambiar la variable
    {
        m_GameObject = characterObj; // transpaso d evariable de character a gameobject
        m_ssb = m_GameObject.GetComponent<SimonSaysBehaviour>(); // asignar componentes de simon sat b a m_ssb
    }

    // here we define the structure of the state machine's first layer
    public override void SetupDefinition(ref FSMStateType stateType, ref List<System.Type> children)
    {
        // default is an OR-type state
        // the first child added will be the inital state
        children.Add(typeof(SimonSaysState_WaitingToPlay));
        children.Add(typeof(SimonSaysState_IncreasingSequence));
        children.Add(typeof(SimonSaysState_PlayingSequence));
        children.Add(typeof(SimonSaysState_UserInputFAIL));
        children.Add(typeof(SimonSaysState_UserInputOK));
        children.Add(typeof(SimonSaysState_WaitingUserInput));
    }
}
