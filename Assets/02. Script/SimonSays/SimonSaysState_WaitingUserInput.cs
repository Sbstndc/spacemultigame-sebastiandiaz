﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_WaitingUserInput : BaseFSMState
{
    public float m_sss_wui_fT; // variable local de tiempo
    public float m_sss_wui_lOF = 1.0f; // variable local de light off
    public int m_sss_wui_aL;//activeLight

    public override void Enter()
    {
        // declarar SM variable de componentes de padre de SimonSaysBehaviourStateMachine
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
        SM.m_ssb.info.text = "Esperando tu eleccion para jugar secuencia...";//waiting for user to play sequence...
        SM.m_ssb.m_ssb_lOff();
        SM.m_ssb.buttonPressed = -1;
        SM.m_ssb.currentSequenceIndex = 0;
        m_sss_wui_fT = 0.0f;
        m_sss_wui_aL = -1;
    }

    public override void Exit()
    { }

    public override void Update()
    {
        m_sss_wui_fT = m_sss_wui_fT + Time.deltaTime;//acumular tiempo de delta time
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
        //si el tiempo acumulado del delta time en variable local es mayo a light off
        //y active light es distinto de -1, entonces
        if(m_sss_wui_fT > m_sss_wui_lOF && m_sss_wui_aL != -1)
        {
            SM.m_ssb.m_ssb_lOff();// llamar a funcion light off de padre
            m_sss_wui_aL = -1; // asignar -1 a active light
        }

        //    if (buttonPressed != -1)
        if(SM.m_ssb.buttonPressed != -1)
        {
            //        PressButton(buttonPressed);
            m_sss_wui_aL = SM.m_ssb.buttonPressed;
            m_sss_wui_fT = 0.0f;
            //        if (buttonPressed != sequence[currentSequenceIndex])
            if (SM.m_ssb.buttonPressed  !=  SM.m_ssb.sequence[SM.m_ssb.currentSequenceIndex])
            {
                DoTransition(typeof(SimonSaysState_UserInputFAIL));    
            }
            //        else if (currentSequenceIndex == sequence.Count - 1)
            else if (SM.m_ssb.currentSequenceIndex == SM.m_ssb.sequence.Count -1)
            {
                DoTransition(typeof(SimonSaysState_UserInputOK));
            }
            else
            {
                Debug.Log("SM.m_ssb.buttonPressed: " + SM.m_ssb.buttonPressed);
                SM.m_ssb.buttonPressed = -1;
                SM.m_ssb.currentSequenceIndex++;
                Debug.Log("La index secuencua actual es>" + SM.m_ssb.currentSequenceIndex);
                Debug.Log("La secuencua contador es>" + SM.m_ssb.sequence.Count); 
            }
            // reseteamo el bottonpresset a -1, que es distinto a las opciones(0,1,2,3)
            //de esta forma evitamos que quede con el ultimo valor el button pressed
        }

        //    if (m_SM.IsFirstTime())
        //    {
        //        //Apagar primer totes les llums:
        //        foreach (GameObject light in lights)
        //        {
        //            light.SetActive(false);
        //        }

        //        info.text = "Es tu Turno...";
        //        buttonPressed = -1;
        //        currentSequenceIndex = 0;
        //    }

        //    if (buttonPressed != -1)
        //    {
        //        PressButton(buttonPressed);
        //        if (buttonPressed != sequence[currentSequenceIndex])
        //        {
        //            m_SM.ChangeState(SimonSays_State.UserInputFAIL);
        //        }
        //        else if (currentSequenceIndex == sequence.Count - 1)
        //        {
        //            m_SM.ChangeState(SimonSays_State.UserInputOK);
        //        }
        //        else
        //        {
        //            buttonPressed = -1;
        //            currentSequenceIndex++;
        //        }
        //    }
        //}
    }
}
