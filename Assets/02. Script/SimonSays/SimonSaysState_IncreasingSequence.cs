﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_IncreasingSequence : BaseFSMState
{
    public override void Enter()
    {
        //obtener la maquina de estados de SimonSaysBehaviourStateMachine a travez de SM
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
        //SM es Simon Says Maquina de estados
        int sss_is_nB = Random.Range(0, 4); //nB es variable nuevo boton obtenido de un random

        SM.m_ssb.info.text = "Incrementando Secuencia..."; // enviar mensaje a text a travez de sm

        SM.m_ssb.sequence.Add(sss_is_nB); //a;adir nuevo boton a la secuencia a travez de Sm

    }

    public override void Exit()
    { }

    public override void Update()
    {
        DoTransition(typeof(SimonSaysState_PlayingSequence));

        //SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
        ////SM.m_ssb.info.text = "CORRECTO!!!!! :)"; 
        //int newButton = Random.Range(0, 4);
        //sequence.Add(newButton);
        ////m_SM.ChangeState(SimonSays_State.PlayingSequence);
    }
}

