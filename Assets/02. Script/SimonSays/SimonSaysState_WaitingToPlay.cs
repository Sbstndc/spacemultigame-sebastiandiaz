﻿using UnityEngine;
using System.Collections;
using FSMHelper;//librerira de ayuda de finita state machine

public class SimonSaysState_WaitingToPlay : BaseFSMState
{
    public override void Enter()
    {
        // declarar Sm variable de padre state machine
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();

        //enviar a text el siguiente mensaje a travez de padre SM.m...
        SM.m_ssb.info.text = "Preciona la Tecla 'Space' para Comenzar";
    }

    public override void Exit() // 
    { }

    public override void Update()
    {
        //obtener paleta de variables del nodo padre
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();

        //si el imput presionado es @Espacio@
        if(Input.GetKeyDown(KeyCode.Space))
        {
            //impiar secuencia
            SM.m_ssb.sequence.Clear();
            //siguiente secuencia
            DoTransition(typeof(SimonSaysState_IncreasingSequence));

        }
        //else
        //{
        //    SM.m_ssb.m_ssb_Wtp(index);
        //}

        //if (m_SM.IsFirstTime())
        //{
        //    info.text = "Esperando para Jugar, Presiona Space Para Comenzar";
        //}

        //if (Input.GetKeyDown(KeyCode.Space))
        //{
        //    m_SM.ChangeState(SimonSaysBehaviourStateMachine.IncreasingSequence);
        //    sequence.Clear();
        //}
    }
}
