﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_PlayingSequence : BaseFSMState
{
    public float m_sss_ps_lC = 1.5f; // ligt Change
    public float m_sss_ps_lOff = 1.0f; // Light Off
    public float m_sss_ps_fT = 0f; //variable local de tiempo
    
    public override void Enter()
    {
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
        SM.m_ssb.info.text = "Reproduciendo Secuencia...";
        SM.m_ssb.currentSequenceIndex = 0; // definimos la secuencia como 0 a travez de la Sm del padre
    }
    public override void Exit()
    { }

    public override void Update()
    {
        m_sss_ps_fT = m_sss_ps_fT + Time.deltaTime;//acumular tiempo de delta time
        //acceder a las variables del padre a travez de SM
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
        // si el tiempo de delta time acumulado es mayor al tiempo de apagado y si el index de la actual 
        // secuencia es menor que el contador de la secuencia
        if (SM.m_ssb.currentSequenceIndex < SM.m_ssb.sequence.Count && m_sss_ps_fT > m_sss_ps_lOff)//si es la primera vez que entra index=0, seugnda vez 
        {
            SM.m_ssb.m_ssb_lOff();//apagar luces
        }

        if (SM.m_ssb.currentSequenceIndex < SM.m_ssb.sequence.Count && m_sss_ps_fT > m_sss_ps_lC)
        {
            SM.m_ssb.PressButton(SM.m_ssb.sequence[SM.m_ssb.currentSequenceIndex]);

            SM.m_ssb.currentSequenceIndex++; // incrementar secuencia del actual secuencia index
            //currentSequenceIndex=currentSequenceIndex + 1
            m_sss_ps_fT = 0f;//resetear el tiempo a 0 
        }
        else if (SM.m_ssb.currentSequenceIndex >= SM.m_ssb.sequence.Count && m_sss_ps_fT > m_sss_ps_lC)
        {
            DoTransition(typeof(SimonSaysState_WaitingUserInput));

        }
        //if (m_SM.IsFirstTime())
        //{
        //    info.text = "Ejecutando Secuencua...";
        //    currentSequenceIndex = 0;
        //    PressButton(sequence[currentSequenceIndex]);
        //}

        //if (m_SM.m_fTime > 2f)
        //{
        //    currentSequenceIndex++;
        //    if (currentSequenceIndex >= sequence.Count)
        //    {
        //        m_SM.ChangeState(SimonSays_State.WaitingUserInput);
        //    }
        //    else
        //    {
        //        PressButton(sequence[currentSequenceIndex]);
        //        m_SM.m_fTime = 0f;
        //    }
        //}
    }
}

