﻿
using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_UserInputFAIL : BaseFSMState
{
    public float m_sss_uif_fT;//variable local de ftime

    public override void Enter()
    {
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
        SM.m_ssb.info.text = "ERROR!!!!!!";
        SM.m_ssb.currentSequenceIndex = 0;// obtener secuencia
        m_sss_uif_fT = 0;//inicializar la variable local en 0 float
    }

    public override void Exit()
    { }

    public override void Update()
    {
        // acumulacio nde tiempo delta time para pasarlo a segundo

        m_sss_uif_fT = m_sss_uif_fT + Time.deltaTime; 
        //si tiempo deltatime en segundos es mayor que 2 float, entocnes
        if (m_sss_uif_fT > 2.0f)
        {
            //llamada de variable de padre SM
            SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
            SM.m_ssb.m_ssb_lOff(); // usar funcion lightOff() a travez del padre y apagar luces
            DoTransition(typeof(SimonSaysState_WaitingToPlay));//pasar a siguiente secuencia
        }
        //if (m_SM.IsFirstTime())
        //{
        //    info.text = "FALLASTE!!!!!  :(";
        //}

        //if (m_SM.m_fTime > 2.0f)
        //{
        //    m_SM.ChangeState(SimonSays_State.WaitingToPlay);
        //}
    }
}
