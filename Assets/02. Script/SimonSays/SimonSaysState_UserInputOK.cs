﻿using UnityEngine;
using System.Collections;
using FSMHelper;

public class SimonSaysState_UserInputOK : BaseFSMState
{
    public float m_sss_uio_fT = 0.0f;//variable local de ftime

    public override void Enter()
    {
        SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
      
        SM.m_ssb.info.text = "PERFECTO!!!!!!";
        
        SM.m_ssb.currentSequenceIndex = 0;
        
        m_sss_uio_fT = 0;
    }

    public override void Exit()
    { }

    public override void Update()
    {
        m_sss_uio_fT = m_sss_uio_fT + Time.deltaTime;
       
        if(m_sss_uio_fT > 2.0f)
        {
            //llamada de variable de padre SM
            SimonSaysBehaviourStateMachine SM = (SimonSaysBehaviourStateMachine)GetStateMachine();
            
            SM.m_ssb.m_ssb_lOff(); // usar lightOff a travez del padre
            
            DoTransition(typeof(SimonSaysState_IncreasingSequence));//
        }

        //    SM.m_ssb.info.text = "CORRECTO!!!!! :)"; // mas  adelante colocar, un arreglo con random para multiples mensajes 

        //    if (SM.m_ssb.m_fTime > 2.0f)
        //    {
        //        m_SM.ChangeState(SimonSays_State.IncreasingSequence);
        //    }
        //}
    }
}